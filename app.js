const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const fs = require('fs')

const config = require('./config')
const task = require('./task')
task.start()

const indexRouter = require('./routes/index')

const app = express()

//创建目录
fs.existsSync(`${config.upload.publishDir}/log`) || fs.mkdirSync(`${config.upload.publishDir}/log`)
fs.existsSync(`${config.upload.publishDir}/tmp`) || fs.mkdirSync(`${config.upload.publishDir}/tmp`)

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = config.isProcEnv ? {} : err

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
