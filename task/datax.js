const fs = require('fs')
const nodeUtil = require('util');
const util = require('../util')
const config = require('../config')

const logger = util.getLogger('task.datax')
const writeFile = nodeUtil.promisify(fs.writeFile);   //写文件

const run = async (jobGroup, jobId) => {
  let filePath = `${config.upload.publishDir}/tmp/datax-${jobId}-${jobGroup.id}.json`
  let logFile = `${config.upload.publishDir}/log/datax-${jobId}-${jobGroup.id}.log`
  //配置主鹰爪如果不存在，就把mongodb中保存的配置写入文件，如果已存在，使用现有文件即可
  // if (! (await util.exists(filePath))) {
  //   logger.debug('配置文件不存在，重新创建:', jobGroup.args)
  //   await writeFile(filePath, JSON.stringify(jobGroup.args, null, 4))
  // }

  //每次都重新创建
  await writeFile(filePath, JSON.stringify(jobGroup.args, null, 4))
  let commond = `python ${config.dataxHome}/bin/datax.py ${filePath} > ${logFile}`
  logger.info(`运行命令: ${commond}`)
  let out = await util.runExec(commond)
  logger.debug(`命令 ${commond} ，输出：`, out)
  return { datax: `${config.upload.urlPrefix}/log/datax-${jobId}-${jobGroup.id}.log` }
}

//删除任务，空函数
const remove = (jobGroup, jobId) => {
}

// 向livy提交 spark 任务，并定时查询运行状态与日志
module.exports = {
  run, remove
}