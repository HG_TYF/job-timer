const mongo = require('../db_helper')
mongo.connect().then( async () => {
    let jobs = (await mongo.jobs.find({status: '1'}).toArray())
    .map(x => {
      let streamingJobArray = x.group.filter(g => g.type === 'streaming')
      if (streamingJobArray && streamingJobArray.length){
        return Object.assign(x, {streaming: streamingJobArray[0]})
      }else{
        return x
      }
    })
    .filter(x => x.streaming && x.streaming.autoRun)
    console.log(jobs)
    const streaming = require('../task/streaming')
})
