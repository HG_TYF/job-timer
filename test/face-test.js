const rp = require('request-promise-native');
const fs = require('fs');

const file2base64 = (filePath) => {
  let data = fs.readFileSync(filePath);
  return Buffer(data).toString('base64');
}

let cardpic = './zw.jpg'
let facepic = './xc.png'
let idcard = '412345789655852145'
let timeStamp = Math.round(new Date().getTime() / 1000).toString()
let authString = 'jhkj' + idcard + timeStamp

let options = {
  uri: 'http://10.57.98.198:9999/PoliceAuthenFace/faceAuthREQ.do',
  method: 'post',
  headers: {
    'Content-Type': 'application/json'
  },
  body: {
    timeStamp, idcard, authString,
    cardpic: file2base64(cardpic),
    facepic: file2base64(facepic),
  },
  json: true // Automatically stringifies the body to JSON
}
// console.log(`request options : `, options)

rp(options).then(res => {
  console.log('response:', res)
}).catch(e => {
  console.log('error:', e)
})
